import { Fragment, useState, useEffect } from 'react'
import CourseCard from "./CourseCard";

export default function UserView({coursesData}) {

    // console.log(coursesData);

    const [courses, setCourses] = useState([]);

    useEffect(() => {
        
        // Map through the courses received from the parent component (courses page) to render the course cards
        const coursesArr = coursesData.map(course => {
            // Returns active courses as "CourseCard" components
        	if(course.isActive === true){
				return (
					<CourseCard courseProp={course} key={course._id}/>
				)
        	}else{
        		return null;
        	}
        });

        // Set the "courses" state with the course card components returned by the map method
        // Allows the course card components to be rendered in this "UserView" component via the return statement below
        setCourses(coursesArr);

    }, [coursesData]);

    return(
        <Fragment>
            {courses}
        </Fragment>
    );
}
