import React, { useState, useEffect } from 'react';

import Container from 'react-bootstrap/Container';

export default function Counter(){
	const [counter, setCounter] = useState(0);

	useEffect(()=>{
		setCounter(counter+1);
		console.log(counter);
	},[])

	return(
		<Container>
			<p>You clicked ${counter} times</p>
			
		</Container>
	)
}